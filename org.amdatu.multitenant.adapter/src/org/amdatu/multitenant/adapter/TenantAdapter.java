/*
 * Copyright (c) 2010-2012 The Amdatu Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.multitenant.adapter;

import static org.amdatu.multitenant.adapter.Constants.DEFAULT_SCOPE_FILTER;
import static org.amdatu.multitenant.adapter.Constants.MULTITENANT_BUNDLE_ACTIVATOR_KEY;
import static org.amdatu.multitenant.adapter.Constants.MULTITENANT_SCOPE_KEY;
import static org.amdatu.multitenant.adapter.Constants.TENANT_PID_PLACEHOLDER;

import java.io.File;

import org.amdatu.multitenant.Tenant;
import org.apache.felix.dm.Component;
import org.apache.felix.dm.DependencyManager;
import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import org.osgi.framework.Constants;
import org.osgi.service.log.LogService;

/**
 * Adapter that instantiates a bundle activator and makes that instance tenant specific
 * by intercepting the bundle context.
 * 
 * @author <a href="mailto:amdatu-developers@amdatu.org">Amdatu Project Team</a>
 */
public class TenantAdapter implements BundleDataStore {

    private volatile BundleContext m_context;
    private volatile Tenant m_tenant;
    private volatile LogService m_log;
    private volatile BundleDataStore m_bundleDataStore;

    private String m_bundleActivatorClass;
    private BundleActivator m_tenantBundleActivator;
    private TenantAwareBundleContext m_tenantAwareBundleContext;

    public void init(Component component) {
        // we add an extra dependency here on the bundle data store so our adapter will
        // wait until it is available
        DependencyManager dm = component.getDependencyManager();
        component.add(dm
            .createServiceDependency()
            .setService(
                BundleDataStore.class,
                "(&(" + org.amdatu.multitenant.Constants.PID_KEY + "=" + getTenantPID() + ")("
                    + org.amdatu.multitenant.adapter.Constants.BUNDLE_ID + "="
                    + m_context.getBundle().getBundleId() + "))")
            .setInstanceBound(true)
            .setRequired(true)
            );
    }

    /**
     * Called by Felix dependency manager when this service adapter is started.
     */
    public void start() {
        m_log.log(LogService.LOG_DEBUG, "[" + getTenantPID() + "] Starting " + getBundleHeader(Constants.BUNDLE_NAME));

        m_bundleActivatorClass = getBundleHeader(MULTITENANT_BUNDLE_ACTIVATOR_KEY);
        if (m_bundleActivatorClass == null) {
            m_log.log(LogService.LOG_ERROR, "Missing manifest header " + MULTITENANT_BUNDLE_ACTIVATOR_KEY);
            return;
        }

        try {
            m_tenantBundleActivator =
                (BundleActivator) m_context.getBundle().loadClass(m_bundleActivatorClass).newInstance();

            m_tenantAwareBundleContext =
                new TenantAwareBundleContext(m_context, getTenantPID(), this, getTenantLookupFilter(),
                    null /* visibilityFilter */);

            m_tenantBundleActivator.start(m_tenantAwareBundleContext);
        }
        catch (Exception e) {
            m_log.log(LogService.LOG_ERROR, "Could not start activator for tenant " + getTenantPID(), e);
        }
    }

    /**
     * Called by Felix dependency manager when this service adapter is stopped.
     */
    public void stop() {
        m_log.log(LogService.LOG_DEBUG, "[" + getTenantPID() + "] Stopping " + getBundleHeader(Constants.BUNDLE_NAME));

        try {
            if (m_tenantBundleActivator != null) {
                m_tenantBundleActivator.stop(m_tenantAwareBundleContext);
            }
        }
        catch (Exception e) {
            m_log.log(LogService.LOG_ERROR, "Could not stop activator for tenant " + getTenantPID(), e);
        }
    }

    public File getRoot() {
        return m_bundleDataStore.getRoot();
    }

    /**
     * Returns a service lookup filter that finds all services matching either
     * <ol>
     * <li>as a tenant-aware service specific for this tenant; or</li>
     * <li>is not a tenant-aware service at all; or</li>
     * <li>is flagged to be global visible.</li>
     * </ol>
     * 
     * @return a filter string, never <code>null</code>.
     */
    private String getTenantLookupFilter() {
        String filter = getBundleHeader(MULTITENANT_SCOPE_KEY);
        if (filter == null || "".equals(filter.trim())) {
            filter = DEFAULT_SCOPE_FILTER;
        }
        return filter.replaceAll(TENANT_PID_PLACEHOLDER, getTenantPID());
    }

    /**
     * @return the tenant's PID, never <code>null</code>.
     */
    private String getTenantPID() {
        return m_tenant.getPID();
    }

    /**
     * @param key the key to retrieve from the bundle header.
     * @return the bundle header's value associated to the given key, can be <code>null</code>.
     */
    private String getBundleHeader(String key) {
        return (String) m_context.getBundle().getHeaders().get(key);
    }
}
