/*
 * Copyright (c) 2010-2012 The Amdatu Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.multitenant.itest.test;

import static org.amdatu.multitenant.Constants.NAME_KEY;
import static org.amdatu.multitenant.Constants.PID_KEY;
import static org.amdatu.multitenant.Constants.PID_VALUE_PLATFORM;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Properties;
import java.util.Set;
import java.util.concurrent.TimeUnit;




import org.amdatu.bndtools.test.BaseOSGiServiceTest;
import org.amdatu.multitenant.Constants;
import org.amdatu.multitenant.Tenant;
import org.amdatu.multitenant.TenantFactoryConfiguration;
import org.amdatu.multitenant.TenantLifeCycleListener;
import org.amdatu.multitenant.itest.MyDependencyService;
import org.amdatu.multitenant.itest.MyDependentService;
import org.amdatu.multitenant.itest.MyGlobalService;
import org.apache.felix.dm.Component;
import org.apache.felix.dm.DependencyManager;
import org.osgi.framework.Bundle;
import org.osgi.framework.BundleContext;
import org.osgi.framework.FrameworkUtil;
import org.osgi.framework.InvalidSyntaxException;
import org.osgi.framework.ServiceReference;
import org.osgi.framework.ServiceRegistration;
import org.osgi.service.cm.Configuration;


/**
 * Integration test cases for multi-tenancy.
 *
 * @author <a href="mailto:amdatu-developers@amdatu.org">Amdatu Project Team</a>
 */
public class MultiTenantTest extends BaseOSGiServiceTest<TenantFactoryConfiguration> {

	private static final String SERVICE_NAME = MyDependentService.class.getName();

	private volatile DependencyManager m_manager;
    private final List<Configuration> m_configurations = new ArrayList<Configuration>();
	private TenantConfigurator m_tenantConfigurator;
	private final BundleContext m_bundleContext = FrameworkUtil.getBundle(this.getClass()).getBundleContext();

    /**
     * Helper method to count the number of service registrations for a given service-class.
     *
     * @param context the bundle context to use;
     * @param service the service to count;
     * @param filter the optional filter to search for, can be <code>null</code>.
     * @return a service registration count, >= 0.
     */
    static int countServices(BundleContext context, String serviceName, String filter) {
        ServiceReference[] serviceReferences = null;
        try {
            serviceReferences = context.getServiceReferences(serviceName, filter);
        }
        catch (InvalidSyntaxException exception) {
            throw new RuntimeException("null-filter is incorrect?!");
        }
        return serviceReferences == null ? 0 : serviceReferences.length;
    }

    /**
     * Helper method to count the number of service registrations for a given service-class.
     *
     * @param context the bundle context to use;
     * @param service the service to count.
     * @return a service registration count, >= 0.
     */
    static int countServices(BundleContext context, String serviceName) {
        return countServices(context, serviceName, null /* filter */);
    }


    /**
     * Sets up an individual test case.
     *
     * @throws Exception not part of this set up.
     */
    public MultiTenantTest() {
		super(TenantFactoryConfiguration.class);
	}
    
    public void setUp() throws Exception
    {
    	super.setUp();
     	m_tenantConfigurator = new TenantConfigurator();
    	m_manager.add(m_manager.createComponent().setImplementation(m_tenantConfigurator).add(m_manager.createServiceDependency().setService(TenantFactoryConfiguration.class).setRequired(true)));
    	m_tenantConfigurator.configureTenants(Constants.PID_VALUE_PLATFORM, "Default");
    }
 
    /**
     * Tears down an individual test case.
     */
    public void tearDown() {
        for (Configuration config : m_configurations) {
            try {
                config.delete();
            }
            catch (Exception exception) {
                // Ignore...
            }
        }
        m_configurations.clear();
       // m_testContext.tearDown();
    }

    /**
     * Used to monitor the life cycle of tenants.
     */
    public static class Listener implements TenantLifeCycleListener {
        private final Set<String> m_tenants = new HashSet<String>();

        public Listener() {
        }

        public void initial(String[] tenantPids) {
            synchronized (m_tenants) {
                m_tenants.clear();
                for (String pid : tenantPids) {
                    m_tenants.add(pid);
                }
            }
        }

        public void create(String tenantPid) {
            synchronized (m_tenants) {
                m_tenants.add(tenantPid);
            }
        }

        public void delete(String tenantPid) {
            synchronized (m_tenants) {
                m_tenants.remove(tenantPid);
            }
        }

        public String[] getTenants() {
            synchronized (m_tenants) {
                return m_tenants.toArray(new String[m_tenants.size()]);
            }
        }
    }
    
    /**
     * Tests that the tenant life cycle matches the number of configurations that are added and removed.
     */
    public void testTenantLifeCycle() throws Exception {

        Listener initialListener = new Listener();
        Listener secondListener = new Listener();

    

        m_manager.add(m_manager.createComponent()
            .setInterface(TenantLifeCycleListener.class.getName(), null)
            .setImplementation(initialListener));

        String pid1 = generateTenantPID();
        m_tenantConfigurator.configureTenants(Constants.PID_VALUE_PLATFORM, "Default", pid1);
        assertEquals(3, initialListener.getTenants().length);
        assertTrue(Arrays.asList(initialListener.getTenants()).contains(pid1));

        String pid2 = generateTenantPID();
        m_tenantConfigurator.configureTenants(Constants.PID_VALUE_PLATFORM, "Default", pid1, pid2);
        assertEquals(4, initialListener.getTenants().length);
        assertTrue(Arrays.asList(initialListener.getTenants()).contains(pid1));
        assertTrue(Arrays.asList(initialListener.getTenants()).contains(pid2));

        m_manager.add(m_manager.createComponent()
            .setInterface(TenantLifeCycleListener.class.getName(), null)
            .setImplementation(secondListener));

        assertEquals(4, secondListener.getTenants().length);
        assertTrue(Arrays.asList(secondListener.getTenants()).contains(pid1));
        assertTrue(Arrays.asList(secondListener.getTenants()).contains(pid2));

        m_tenantConfigurator.configureTenants(Constants.PID_VALUE_PLATFORM, "Default", pid2);
        assertEquals(3, initialListener.getTenants().length);
        assertFalse(Arrays.asList(initialListener.getTenants()).contains(pid1));
        assertTrue(Arrays.asList(initialListener.getTenants()).contains(pid2));

        assertEquals(3, secondListener.getTenants().length);
        assertFalse(Arrays.asList(secondListener.getTenants()).contains(pid1));
        assertTrue(Arrays.asList(secondListener.getTenants()).contains(pid2));

        m_tenantConfigurator.configureTenants(Constants.PID_VALUE_PLATFORM, "Default");
        assertEquals(2, initialListener.getTenants().length);
        assertFalse(Arrays.asList(initialListener.getTenants()).contains(pid2));
        assertEquals(2, secondListener.getTenants().length);
        assertFalse(Arrays.asList(secondListener.getTenants()).contains(pid2));
    }

    /**
     * Tests the persistency of a tenant lifecycle.
     *
     * @throws Exception not part of this test case.
     */
    public void testTenantLifeCyclePersistent() throws Exception {
        Listener listener = new Listener();

        DependencyManager dm = m_manager;
        Component component =
            dm.createComponent()
                .setInterface(TenantLifeCycleListener.class.getName(), null)
                .setImplementation(listener);
        dm.add(component);

        String pid1 = generateTenantPID();
        m_tenantConfigurator.configureTenants(Constants.PID_VALUE_PLATFORM, "Default", pid1);
        assertEquals(3, listener.getTenants().length);
        assertTrue(Arrays.asList(listener.getTenants()).contains(pid1));

        Bundle tenantFactory = null;
        for (Bundle bundle : m_bundleContext.getBundles()) {
            if ("org.amdatu.multitenant.factory".equals(bundle.getSymbolicName())) {
                tenantFactory = bundle;
            }
        }
        assertNotNull(tenantFactory);
       
        tenantFactory.stop();
        tenantFactory.start();

        //
        assertEquals(3, listener.getTenants().length);
        m_tenantConfigurator.configureTenants(Constants.PID_VALUE_PLATFORM, "Default");
        m_tenantConfigurator.waitForSystemToSettle();
        // but now we should
        assertEquals(2, listener.getTenants().length);
        Arrays.asList(listener.getTenants()).contains(pid1);
    }

    /** Tests a listener that binds just to the platform, so it should not react to tenants being added. */
    public void testTenantLifeCycleBindingPlatform() throws Exception {
        Listener listener = new Listener();
        DependencyManager dm =m_manager;
        Component component = dm.createComponent()
            .setInterface(TenantLifeCycleListener.class.getName(), new Properties() {{ put(org.amdatu.multitenant.Constants.MULTITENANT_LIFECYCLELISTENER_BINDING_KEY, org.amdatu.multitenant.Constants.MULTITENANT_LIFECYCLELISTENER_BINDING_PLATFORM); }})
            .setImplementation(listener);
        dm.add(component);

        assertEquals(1, listener.getTenants().length);
        Configuration tc1 = addTenantConfig(createTenantConfiguration(generateTenantPID()));
        assertEquals(1, listener.getTenants().length);
        removeTenantConfig(tc1);
        assertEquals(1, listener.getTenants().length);
    }

    /** Tests a listener that binds just to tenants, so it should ignore the platform tenant. */
    public void testTenantLifeCycleBindingTenants() throws Exception {
        Listener listener = new Listener();
        DependencyManager dm =m_manager;
        Component component = dm.createComponent()
            .setInterface(TenantLifeCycleListener.class.getName(), new Properties() {{ put(org.amdatu.multitenant.Constants.MULTITENANT_LIFECYCLELISTENER_BINDING_KEY, org.amdatu.multitenant.Constants.MULTITENANT_LIFECYCLELISTENER_BINDING_TENANTS); }})
            .setImplementation(listener);
        dm.add(component);

        assertEquals(1, listener.getTenants().length);
        m_tenantConfigurator.configureTenants(Constants.PID_VALUE_PLATFORM, "Default", generateTenantPID());
        assertEquals(2, listener.getTenants().length);
        m_tenantConfigurator.configureTenants(Constants.PID_VALUE_PLATFORM, "Default");
        assertEquals(1, listener.getTenants().length);
    }

    /** Tests a listener that binds to both the platform and other tenants. */
    public void testTenantLifeCycleBindingBoth() throws Exception {
        Listener listener = new Listener();
        DependencyManager dm = m_manager;
        Component component = dm.createComponent()
            .setInterface(TenantLifeCycleListener.class.getName(), new Properties() {{ put(org.amdatu.multitenant.Constants.MULTITENANT_LIFECYCLELISTENER_BINDING_KEY, org.amdatu.multitenant.Constants.MULTITENANT_LIFECYCLELISTENER_BINDING_BOTH); }})
            .setImplementation(listener);
        dm.add(component);

        assertEquals(2, listener.getTenants().length);
        m_tenantConfigurator.configureTenants(Constants.PID_VALUE_PLATFORM, "Default", generateTenantPID());
        assertEquals(3, listener.getTenants().length);
        m_tenantConfigurator.configureTenants(Constants.PID_VALUE_PLATFORM, "Default");
        assertEquals(2, listener.getTenants().length);
    }

    /** Tests a (theoretical) listener that binds to neither the platform not other tenants. */
    public void testTenantLifeCycleBindingNone() throws Exception {
        Listener listener = new Listener();
        DependencyManager dm = m_manager;
        Component component = dm.createComponent()
            .setInterface(TenantLifeCycleListener.class.getName(), new Properties() {{ put(org.amdatu.multitenant.Constants.MULTITENANT_LIFECYCLELISTENER_BINDING_KEY, 0); }})
            .setImplementation(listener);
        dm.add(component);

        assertEquals(0, listener.getTenants().length);
        Configuration tc1 = addTenantConfig(createTenantConfiguration(generateTenantPID()));
        assertEquals(0, listener.getTenants().length);
        removeTenantConfig(tc1);
        assertEquals(0, listener.getTenants().length);
    }

    /**
     * Tests that when a multi-tenant aware service is registered, multiple service registrations are made, one for each tenant.
     *
     * @throws Exception not part of this test case.
     */
    public void testAddingMultiTenantAwareServiceCausesNewServiceRegistrations() throws Exception {
        String tenantPID = generateTenantPID();

        Properties properties = createTenantConfiguration(tenantPID);
        addTenantConfig(properties);

        // We've got a number of explicit tenants + one framework (= default) tenant
        int expectedServiceCount = getConfigurationCount();

        // A non-MT aware bundle should be able to see *all* service registrations...
        assertEquals(expectedServiceCount, countServices(m_bundleContext, SERVICE_NAME));
    }

    /**
     * Tests that when a multi-tenant aware service is registered, multiple service registrations are made, one for each tenant.
     *
     * @throws Exception not part of this test case.
     */
    public void testMultiTenantAwareServiceCausesMultipleServiceRegistrations() throws Exception {
        // We've got a number of explicit tenants + one framework (= default) tenant
        // A non-MT aware bundle should be able to see *all* service registrations...
        assertEquals(2, countServices(m_bundleContext, SERVICE_NAME));
    }

    /**
     * Tests that when a multi-tenant aware service is registered, each tenant only sees its own services, not the services of other tenants.
     *
     * @throws Exception not part of this test case.
     */
    public void testMultiTenantAwareServiceOnlySeesItsOwnServices() throws Exception {
        int newCount;

        String tenantPID = generateTenantPID();

        assertEquals(0, countServices(m_bundleContext, MyDependentService.class.getName(),
            String.format("(%s=%s)", PID_KEY, tenantPID)));

        m_tenantConfigurator.configureTenants(Constants.PID_VALUE_PLATFORM, "Default", tenantPID);

        newCount = countServices(m_bundleContext, MyDependentService.class.getName(),
            String.format("(%s=%s)", PID_KEY, tenantPID));
        assertEquals(1, newCount);

        newCount = countServices(m_bundleContext, MyDependencyService.class.getName(),
            String.format("(%s=%s)", PID_KEY, tenantPID));
        assertEquals(1, newCount);
    }

    /**
     * Tests that you can retrieve the multi-tenant-aware <em>tenant</em> service if you explicitly ask for it.
     *
     * @throws Exception not part of this test case.
     */
    public void testObtainTenantSpecificServiceInstanceSucceedsForNonMultiTenantService() throws Exception {
        String tenantPID = generateTenantPID();

        m_tenantConfigurator.configureTenants(Constants.PID_VALUE_PLATFORM, "Default", tenantPID);
        
        ServiceReference[] serviceRefs = m_bundleContext.getServiceReferences(SERVICE_NAME,
            String.format("(%1$s=%2$s)", PID_KEY, tenantPID));
        assertNotNull(serviceRefs);
        assertEquals(1, serviceRefs.length);

        MyDependentService service = (MyDependentService) m_bundleContext.getService(serviceRefs[0]);
        assertEquals(String.format("[%1$s] [%1$s]", tenantPID), service.sayIt());
    }

    /**
     * Tests that a global visible service is marked as such in its service properties.
     *
     * @throws Exception not part of this test case.
     */
    public void testGlobalVisibleServiceOk() throws Exception {
        ServiceReference[] serviceRefs = m_bundleContext.getServiceReferences(MyGlobalService.class.getName(),
            String.format("(%1$s=%2$s)", PID_KEY, PID_VALUE_PLATFORM));
        assertNotNull(serviceRefs);
        assertEquals(1, serviceRefs.length);

        assertEquals(null, serviceRefs[0].getProperty("org.amdatu.tenant.global"));

        serviceRefs = m_bundleContext.getServiceReferences(MyDependencyService.class.getName(),
            String.format("(%1$s=%2$s)", PID_KEY, PID_VALUE_PLATFORM));
        assertNotNull(serviceRefs);
        assertEquals(1, serviceRefs.length);

        assertEquals(null, serviceRefs[0].getProperty("org.amdatu.tenant.global"));
    }

    /**
     * Tests that updating the service properties of a globally visible service lets this service remain globally
     * visible (i.e., the global visibility flag is persistent).
     *
     * @throws Exception not part of this test case.
     */
    public void testUpdateServicePropertiesOfGlobalVisibleServiceRemainsGloballyVisibleOk() throws Exception {
        String filter = String.format("(%1$s=%2$s)", PID_KEY, PID_VALUE_PLATFORM);

        ServiceReference[] serviceRefs = m_bundleContext.getServiceReferences(MyGlobalService.class.getName(), filter);
        assertNotNull(serviceRefs);
        assertEquals(1, serviceRefs.length);

        ServiceReference serviceReference = serviceRefs[0];
        assertEquals(null, serviceReference.getProperty("org.amdatu.tenant.global"));

        MyGlobalService service = (MyGlobalService) m_bundleContext.getService(serviceReference);

        ServiceRegistration serviceReg = service.getServiceRegistration();
        assertNotNull(serviceReg);

        Properties dict = new Properties();
        for (String key : serviceReference.getPropertyKeys()) {
            Object value = serviceReference.getProperty(key);
            // Copy all flags of the original service registration but not the global visibility flag...
            if (!"org.amdatu.tenant.global".equals(key)) {
                dict.put(key, value);
            }
        }

        // Update the service registration...
        serviceReg.setProperties(dict);

        // Wait a little while until the service is updated...
        TimeUnit.MILLISECONDS.sleep(250);

        serviceRefs = m_bundleContext.getServiceReferences(MyGlobalService.class.getName(), filter);
        assertNotNull(serviceRefs);
        assertEquals(1, serviceRefs.length);

        assertEquals(null, serviceRefs[0].getProperty("org.amdatu.tenant.global"));
    }

    /**
     * Tests that you can retrieve the multi-tenant-aware <em>platform</em> service if you explicitly ask for it.
     *
     * @throws Exception not part of this test case.
     */
    public void testObtainPlatformSpecificServiceInstanceSucceedsForNonMultiTenantService() throws Exception {
        String tenantPID = generateTenantPID();

        Properties properties = createTenantConfiguration(tenantPID);
        addTenantConfig(properties);

        ServiceReference[] serviceRefs = m_bundleContext.getServiceReferences(SERVICE_NAME,
            String.format("(%1$s=%2$s)", PID_KEY, Constants.PID_VALUE_PLATFORM));
        assertNotNull("Failed to obtain the MT-platform service?!", serviceRefs);
        assertEquals(1, serviceRefs.length);

        MyDependentService service = (MyDependentService) m_bundleContext.getService(serviceRefs[0]);
        assertEquals(String.format("[%1$s] [%1$s]", Constants.PID_VALUE_PLATFORM), service.sayIt());
    }

    /**
     * Tests that when a multi-tenant aware service is registered, multiple service registrations are made, one for each tenant.
     *
     * @throws Exception not part of this test case.
     */
    public void testRemovingMultiTenantAwareServiceCausesServiceDeregistrations() throws Exception {
        String tenantPID = generateTenantPID();

        m_tenantConfigurator.configureTenants(Constants.PID_VALUE_PLATFORM, "Default", tenantPID);
        
        // A non-MT aware bundle should be able to see *all* service registrations...
        assertEquals(3, countServices(m_bundleContext, SERVICE_NAME));

        // Remove configuration; should cause service de-registration for tenant...
        m_tenantConfigurator.configureTenants(Constants.PID_VALUE_PLATFORM, "Default");

        // A non-MT aware bundle should be able to see *all* service registrations...
        assertEquals(2, countServices(m_bundleContext, SERVICE_NAME));
    }

    /**
     * Tests that it is impossible to update the configuration for the PLATFORM-tenant.
     *
     * @throws Exception not part of this test case.
     */
    public void testUpdatePlatformTenantImpossible() throws Exception {
        // We've got a number of explicit tenants + one framework (= default) tenant
        Properties properties = createTenantConfiguration(PID_VALUE_PLATFORM);
        Configuration config = addTenantConfig(properties);
        int expectedServiceCount = getConfigurationCount();

        // ??? configuration should actually be null?!
        assertNotNull(config);

        // A non-MT aware bundle should be able to see *all* service registrations...
        assertEquals(expectedServiceCount, countServices(m_bundleContext, SERVICE_NAME));

        // Check that we can still find back our existing services...
        assertEquals(1, countServices(m_bundleContext, Tenant.class.getName(), "(" + PID_KEY
            + "=" + Constants.PID_VALUE_PLATFORM + ")"));
    }

    /**
     * Tests that when a multi-tenant aware service is updated only its properties are updated.
     *
     * @throws Exception not part of this test case.
     */
    public void testUpdateMultiTenantAwareServiceCausesServiceUpdates() throws Exception {
        String tenantPID = generateTenantPID();

        assertEquals(
            0,
            countServices(m_bundleContext, MyDependentService.class.getName(),
                String.format("(%s=%s)", PID_KEY, tenantPID)));

        String oldTenantName = "The old name";
        m_tenantConfigurator.configureTenants(new TenantWithProperties(Constants.PID_VALUE_PLATFORM), 
            new TenantWithProperties("Default"), 
            new TenantWithProperties(tenantPID, NAME_KEY, oldTenantName));
        
        
        // We've got a number of explicit tenants + one framework (= default) tenant
        // A non-MT aware bundle should be able to see *all* service registrations...
        assertEquals(3, countServices(m_bundleContext, SERVICE_NAME));

        // Verify the tenant can be found by its current name...
        assertEquals(1, countServices(m_bundleContext, Tenant.class.getName(), "(" + PID_KEY
            + "=" + tenantPID + ")"));

        int countBefore =
            countServices(m_bundleContext, MyDependentService.class.getName(),
                String.format("(%s=%s)", PID_KEY, tenantPID));
        
        // Update the configuration...
        String newTenantName = "My cool new name";
        m_tenantConfigurator.configureTenants(new TenantWithProperties(Constants.PID_VALUE_PLATFORM), 
            new TenantWithProperties("Default"), 
            new TenantWithProperties(tenantPID, PID_KEY, "This-value-should-be-ignored", NAME_KEY, newTenantName));

        int countAfter =
            countServices(m_bundleContext, MyDependentService.class.getName(),
                String.format("(%s=%s)", PID_KEY, tenantPID));

        // A non-MT aware bundle should be able to see *all* service registrations...
        assertEquals(3, countServices(m_bundleContext, SERVICE_NAME));
        assertEquals(countBefore, countAfter);

        // Check that we can find back our newly named tenant...
        assertEquals(1, countServices(m_bundleContext, Tenant.class.getName(), "(" + NAME_KEY
            + "=" + newTenantName + ")"));
        // Check that the "old" tenant is really gone...
        assertEquals(0, countServices(m_bundleContext, Tenant.class.getName(), "(" + NAME_KEY
            + "=" + oldTenantName + ")"));
    }

    /**
     * @param tenantPID
     * @return
     */
    private Properties createTenantConfiguration(String tenantPID) {
        Properties properties = new Properties();
        properties.put(PID_KEY, tenantPID);
        properties.put(NAME_KEY, "Tenant " + tenantPID);
        return properties;
    }

    /**
     * @return
     */
    private String generateTenantPID() {
        return String.format("mt%s", Long.toHexString(System.nanoTime()));
    }

    /**
     * Returns the number of configurations.
     *
     * @return the configuration count + 1 for the _PLATFORM tenant service.
     */
    private int getConfigurationCount() {
        return m_configurations.size() + 1;
    }

    /**
     * @param properties
     * @throws Exception
     */
    private void removeTenantConfig(Configuration config) throws Exception {
        m_configurations.remove(config);
        config.delete();
        m_tenantConfigurator.waitForSystemToSettle();
    }

    /**
     * @param properties
     * @throws Exception
     */
    private Configuration addTenantConfig(Properties properties) throws Exception {
        Configuration config = m_tenantConfigurator.updateFactoryConfig("Default", properties);
        m_configurations.add(config);
        m_tenantConfigurator.waitForSystemToSettle();
        return config;
    }

}
