/*
 * Copyright (c) 2010-2012 The Amdatu Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.multitenant.adapter;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import org.amdatu.multitenant.Constants;
import org.amdatu.multitenant.TenantLifeCycleListener;
import org.apache.felix.dm.Component;
import org.apache.felix.dm.DependencyManager;
import org.osgi.framework.BundleContext;

/**
 * Implements a tenants specific data store for bundles, using the life cycle listener.
 */
public class MultiTenantBundleDataStore implements TenantLifeCycleListener {
    private volatile BundleContext m_context;
    private volatile DependencyManager m_dm;
    private Set<String> m_tenants = new HashSet<String>();
    private Map<String, Component> m_services = new HashMap<String, Component>();
    private File m_root;

    public void start() {
        // list all existing files, assuming they're all existing tenants
        m_root = m_context.getDataFile("");
        File[] files = m_root.listFiles();
        for (File file : files) {
            m_tenants.add(file.getName());
        }
    }

    public void initial(String[] tenantPids) {
        for (String tenantPid : tenantPids) {
            create(tenantPid);
        }
        Arrays.sort(tenantPids);
        for (String tenantPid : m_tenants) {
            if (Arrays.binarySearch(tenantPids, tenantPid) < 0) {
                delete(tenantPid);
            }
        }
    }

    public void create(String tenantPid) {
        Properties props = new Properties();
        props.put(Constants.PID_KEY, tenantPid);
        props.put(org.amdatu.multitenant.adapter.Constants.BUNDLE_ID, m_context.getBundle().getBundleId());
        File tenantRoot = new File(m_root, tenantPid);
        if (!tenantRoot.isDirectory() && !tenantRoot.mkdir()) {
            throw new IllegalStateException("Could not create tenant root directory " + tenantRoot);
        }
        Component component = m_dm.createComponent()
            .setInterface(BundleDataStore.class.getName(), props)
            .setImplementation(new BundleDataStoreImpl(tenantRoot));
        boolean added = false;
        synchronized (m_services) {
            if (!m_services.containsKey(tenantPid)) {
                m_services.put(tenantPid, component);
                added = true;
            }
        }
        if (added) {
            m_dm.add(component);
        }
    }

    public void delete(String tenantPid) {
        Component component;
        synchronized (m_services) {
            component = m_services.remove(tenantPid);
        }
        if (component == null) {
            throw new IllegalStateException("Could not find component for tenant " + tenantPid);
        }
        m_dm.remove(component);
        File tenantRoot = new File(m_root, tenantPid);
        try {
            deleteDir(tenantRoot);
        }
        catch (IOException e) {
            throw new IllegalStateException("Could not delete tenant root directory " + tenantRoot, e);
        }
    }

    /** Helper method to recursively delete a directory and its contents. */
    private void deleteDir(File dir) throws IOException {
        File[] files = dir.listFiles();
        for (File file : files) {
            if (file.isDirectory()) {
                deleteDir(file);
            }
            else {
                if (!file.delete()) {
                    throw new IOException("Could not delete file " + file.getAbsolutePath());
                }
            }
        }
    }
}
