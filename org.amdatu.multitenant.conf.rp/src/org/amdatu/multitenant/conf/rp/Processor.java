/*
 * Copyright (c) 2010-2012 The Amdatu Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.multitenant.conf.rp;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.Dictionary;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import org.amdatu.multitenant.Constants;
import org.amdatu.multitenant.TenantFactoryConfiguration;
import org.osgi.framework.BundleContext;
import org.osgi.service.deploymentadmin.spi.DeploymentSession;
import org.osgi.service.deploymentadmin.spi.ResourceProcessor;
import org.osgi.service.deploymentadmin.spi.ResourceProcessorException;
import org.osgi.service.log.LogService;

/**
 * Custom <code>ResourceProcessor</code> for tenant factory configuration
 * files.
 *  
 * @author <a href="mailto:amdatu-developers@amdatu.org">Amdatu Project Team</a>
 */
public class Processor implements ResourceProcessor {
    public static final String PID = "org.amdatu.tenant.conf.rp";

    private volatile BundleContext m_context;
    private volatile LogService m_log;
    
    private DeploymentSession m_session;
    private Map<String, Dictionary> m_tenants = new HashMap<String, Dictionary>();

    private final Object m_lock = new Object();
    private boolean m_initialized;
    private boolean m_commit;
    private TenantFactoryConfiguration m_config;
    
    public void begin(DeploymentSession session) {
        synchronized (m_lock) {
            m_commit = false;
        }
        m_session = session;
        String name = m_session.getSourceDeploymentPackage().getName();
        loadTenants(name);
    }

    public void process(String name, InputStream stream) throws ResourceProcessorException {
        Properties props = new Properties();
        try {
            props.load(stream);
            m_tenants.put(name, props);
        }
        catch (IOException e) {
            throw new ResourceProcessorException(ResourceProcessorException.CODE_OTHER_ERROR, "Failed to parse tenant properties", e);
        }
    }

    public void dropped(String resource) throws ResourceProcessorException {
        m_tenants.remove(resource);
    }

    public void dropAllResources() throws ResourceProcessorException {
        m_tenants.clear();
    }

    public void prepare() throws ResourceProcessorException {
        // nothing to prepare, after we collected the changes via process and drop
        // we are ready to go
    }

    public void commit() {
        TenantFactoryConfiguration config;
        synchronized (m_lock) {
            config = m_config;
            if (config == null) {
                m_commit = true;
            }
        }
        if (config != null) {
            config.update(createTenants(m_tenants));
        }
        String name = m_session.getSourceDeploymentPackage().getName();
        saveTenants(name);
    }

    public void add(TenantFactoryConfiguration config) {
        boolean needsInitialization, needsCommit;
        synchronized (m_lock) {
            m_config = config;
            needsInitialization = !m_initialized;
            m_initialized = true;
            needsCommit = m_commit;
            m_commit = false;
        }
        if (needsInitialization) {
            initTenants();
        }
        if (needsCommit) {
            config.update(createTenants(m_tenants));
        }
    }

    public void rollback() {
        // not much to do here as we do not persist anything until the commit
    }

    public void cancel() {
        // we have no long running operations that we could cancel
    }

    private Map<String, Map<String, Object>> createTenants(Map<String, Dictionary> tenants) {
        Map<String, Map<String, Object>> map = new HashMap<String, Map<String, Object>>();
        for (Dictionary tenant : tenants.values()) {
            Map<String, Object> props = new HashMap<String, Object>();
            Enumeration enumeration = tenant.keys();
            String pid = null;
            while (enumeration.hasMoreElements()) {
                String key = (String) enumeration.nextElement();
                if (Constants.PID_KEY.equals(key)) {
                    pid = (String) tenant.get(Constants.PID_KEY);
                }
                else {
                    props.put(key, tenant.get(key));
                }
            }
            map.put(pid, props);
        }
        return map;
    }
    
    private String[] listTenants() {
        File dir = m_context.getDataFile("");
        return dir.list();
    }

    private void initTenants() {
        for (String name : listTenants()) {
            loadTenants(name);
            m_config.update(createTenants(m_tenants));
        }
    }

    private void loadTenants(String name) {
        FileInputStream fis = null;
        try {
            fis = new FileInputStream(m_context.getDataFile(name));
            ObjectInputStream ois = new ObjectInputStream(fis);
            m_tenants = (Map<String, Dictionary>) ois.readObject();
        }
        catch (FileNotFoundException e) {
            // ignore this, it is perfectly legal for this file to not exist (the first time)
        }
        catch (Exception e) {
            m_log.log(LogService.LOG_ERROR, "Error loading existing tenants for " + name, e);
        }
        finally {
            if (fis != null) {
                try {
                    fis.close();
                }
                catch (IOException e) {
                    m_log.log(LogService.LOG_ERROR, "Could not close file while loading tenants for " + name, e);
                }
            }
        }
    }

    private void saveTenants(String name) {
        FileOutputStream fos = null;
        try {
            fos = new FileOutputStream(m_context.getDataFile(name));
            ObjectOutputStream oos = new ObjectOutputStream(fos);
            oos.writeObject(m_tenants);
        }
        catch (Exception e) {
            m_log.log(LogService.LOG_ERROR, "Could not persist tenants for " + name, e);
        }
        finally {
            if (fos != null) {
                try {
                    fos.close();
                }
                catch (IOException e) {
                    m_log.log(LogService.LOG_ERROR, "Could not close file while persisting tenants for " + name, e);
                }
            }
        }
    }
}
